package net.sandstorm.castlauncher;

import android.app.Presentation;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Display;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian Wing on 2/20/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class WorkspacePresentation extends Presentation{

    public WorkspacePresentation(Context c, Display d){
        super(c, d);
    }

    private Taskbar taskbar;
    private StartMenu startMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources r = getContext().getResources();
        setContentView(R.layout.workspace_classic);

        View rootView = findViewById(R.id.workspace);
        taskbar = new Taskbar(getContext(), rootView);
        startMenu = new StartMenu(getContext(), rootView, taskbar, getAppsList());

        RelativeLayout desktop = (RelativeLayout) findViewById(R.id.desktop);
        desktop.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        //no action necessary
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        //no action necessary
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        //no action necessary
                        break;
                    case DragEvent.ACTION_DROP:
                        //handle the dragged view being dropped over a drop view
                        RelativeLayout view = (RelativeLayout) event.getLocalState();
                        RelativeLayout target = (RelativeLayout) v;
                        View convertView = getLayoutInflater().inflate(R.layout.desktop_item, null);
                        ImageView iv = (ImageView) convertView.findViewById(R.id.desktop_app_icon);
                        iv.setImageDrawable(((ImageView) view.findViewWithTag("icon")).getDrawable());
                        TextView tv = (TextView) convertView.findViewById(R.id.desktop_app_label);
                        tv.setText(((TextView) view.findViewWithTag("label")).getText().toString());
                        convertView.setX(event.getX() - convertView.getWidth());
                        convertView.setY(event.getY() - convertView.getHeight());

                        //Enable dragging of icons on desktop.
                        convertView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                ClipData dragData = ClipData.newPlainText("", "");
                                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(view);
                                view.startDrag(dragData, myShadow, view, 0);
                                return true;
                            }
                        });

                        /*
                        //Enable launching app from desktop icon.
                        convertView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Launch app.
                                PackageManager manager = act.getPackageManager();
                                Intent i = manager.getLaunchIntentForPackage(getAppsList().get(pos).name.toString());
                                taskbar.addToTaskbar(getAppsList().get(pos));
                                act.startActivity(i);
                            }
                        });*/

                        target.addView(convertView);
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        //no action necessary
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        //TODO: Load desktop icons from file.
    }

    /**
     * Override dispatchTouchEvent method to provide closing functionality for the start menu when a touch is detected outside of the bounds of the menu.
     *
     * @param ev The touch event.
     */
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(startMenu.isVisible()) {
            Bounds bounds = startMenu.getBounds();
            if (ev.getY() < bounds.getTop() || ev.getY() > bounds.getBottom() || ev.getX() > bounds.getRight() || ev.getX() < bounds.getLeft()) {
                //TODO: Check if click is not within start menu button.
                startMenu.hide();
            }
        }
    return super.dispatchTouchEvent(ev);
    }

    /**
     * Returns a list of all apps installed on the device as a {@link AppInfo AppInfo} list.
     * @return A list of all apps installed on the device.
     */
    public List<AppInfo> getAppsList(){
        List<AppInfo> appsList = new ArrayList<AppInfo>();
        //Populate appList with all available apps.
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        PackageManager manager = getContext().getPackageManager();
        List<ResolveInfo> allActivities = manager.queryIntentActivities(i, 0);
        for(ResolveInfo info : allActivities){
            AppInfo app = new AppInfo(info.loadLabel(manager), info.activityInfo.packageName, info.activityInfo.loadIcon(manager));
            appsList.add(app);
        }

        return appsList;
    }
}
