package net.sandstorm.castlauncher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian Wing on 2/14/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class Taskbar {

    private Context context;
    private View rootView;
    private List<AppInfo> appsList;

    public Taskbar(Context c, View v){
        context = c;
        rootView = v;
        appsList = new ArrayList<AppInfo>();
    }

    /**
     * Adds an app to the list of apps to be displayed on the taskbar and fills the taskbar with those apps.
     * @param app The app to be added to the taskbar view.
     */
    public void addToTaskbar(AppInfo app){
        //Check if app is already in taskbar list.
        for(int i = 0; i < appsList.size(); i++){
            if(app == null){
                break;
            }
            //Check if the new app and any other app are the same in either name, label, or icon.
            if(appsList.get(i).label.equals(app.label) && appsList.get(i).name.equals(app.name) && appsList.get(i).icon.getConstantState().equals(app.icon.getConstantState())){
                app = null;
            }
        }

        if(app != null){
            appsList.add(app);
        }

        refreshTaskbar();
    }

    /**
     * Refreshes the view of the taskbar to reflect any recent changes and add click listeners for taskbar items.
     */
    private void refreshTaskbar(){
        //Add apps in taskbarAppsList to the taskbar view.
        LinearLayout taskbar = (LinearLayout) rootView.findViewById(R.id.taskbar);
        taskbar.removeAllViews();
        for(int i = 0; i < appsList.size(); i++){
            final View convertView = LayoutInflater.from(context).inflate(R.layout.taskbar_item, null);
            ImageView iv = (ImageView)convertView.findViewById(R.id.taskbar_app_icon);
            iv.setImageDrawable(appsList.get(i).icon);
            TextView tv = (TextView)convertView.findViewById(R.id.taskbar_app_label);
            tv.setText(appsList.get(i).label);

            taskbar.addView(convertView);

            //Launch app when clicked from taskbar.
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppInfo app = new AppInfo();
                    app.icon = ((ImageView) v.findViewById(R.id.taskbar_app_icon)).getDrawable();
                    app.label = ((TextView) v.findViewById(R.id.taskbar_app_label)).getText().toString();

                    for(int i = 0; i < appsList.size(); i++){
                        if(appsList.get(i).label.equals(app.label) && appsList.get(i).icon.getConstantState().equals(app.icon.getConstantState())){
                            app = appsList.get(i);
                        }
                    }

                    //Launch app.
                    if(!app.name.equals("")){
                        PackageManager manager = context.getPackageManager();
                        Intent i = manager.getLaunchIntentForPackage(app.name.toString());
                        context.startActivity(i);
                    }
                }
            });
        }
    }
}
