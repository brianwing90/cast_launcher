package net.sandstorm.castlauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by Brian Wing on 2/14/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class AppInfo {

    public CharSequence label;
    public String name;
    public Drawable icon;

    public AppInfo(){
        label = "";
        name = "";
    }

    public AppInfo(CharSequence label, String name, Drawable icon){
        this.label = label;
        this.name = name;
        this.icon = icon;
    }
}
