package net.sandstorm.castlauncher;

import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.media.MediaRouter;
import android.view.Display;

/**
 * Created by Brian Wing on 2/17/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class CastDisplayListener implements DisplayManager.DisplayListener {

    private Context context;

    public CastDisplayListener(Context c){
        super();
        context = c;
    }

    private Intent intent;
    private int currentDisplayId;

    @Override
    public void onDisplayAdded(int displayId) {
        MediaRouter router = (MediaRouter) context.getSystemService(Context.MEDIA_ROUTER_SERVICE);
        MediaRouter.RouteInfo route = router.getDefaultRoute();
        if (route != null) {
            Display display = route.getPresentationDisplay();
            if (display != null) {
                intent = new Intent(context, PresentationService.class);
                context.startService(intent);
                currentDisplayId = displayId;
            }
        }
    }

    @Override
    public void onDisplayRemoved(int displayId) {
        if(currentDisplayId == displayId && intent != null){
            context.stopService(intent);
        }
    }

    @Override
    public void onDisplayChanged(int displayId) {

    }
}
