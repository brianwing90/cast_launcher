package net.sandstorm.castlauncher;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaRouter;
import android.os.*;
import android.os.Process;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by Brian Wing on 2/15/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class PresentationService extends Service{

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            MediaRouter router = (MediaRouter) getApplicationContext().getSystemService(Context.MEDIA_ROUTER_SERVICE);
            MediaRouter.RouteInfo route = router.getDefaultRoute();
            if (route != null) {
                Display display = route.getPresentationDisplay();
                if(display != null){
                    Context displayContext = getApplicationContext().createDisplayContext(display);
                    WindowManager manager = (WindowManager) displayContext.getSystemService(WINDOW_SERVICE);
                    LayoutInflater inflater = new LayoutInflater(displayContext) {
                        @Override
                        public LayoutInflater cloneInContext(Context newContext) {
                            return null;
                        }
                    };
                    //TODO: Add top level layout here.
                    //View view = new View(displayContext);
                    RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.workspace_classic, null);
                    try {
                        WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, 0, 0, WindowManager.LayoutParams.TYPE_TOAST, 0, PixelFormat.OPAQUE);
                        params.setTitle("Cast Launcher Desktop");
                        params.gravity = Gravity.RIGHT | Gravity.TOP;
                        manager.addView(view, params);
                    }catch(WindowManager.BadTokenException e){
                        Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    new Workspace(displayContext, display);
                }
            }

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("PresentationService", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_LONG).show();

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_LONG).show();
    }
}