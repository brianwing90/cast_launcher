package net.sandstorm.castlauncher;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.media.MediaRouter;
import android.view.Display;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Created by Brian Wing on 2/20/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class CastLauncherMediaRouterCallback extends MediaRouter.SimpleCallback{

    private Activity activity;
    private WorkspacePresentation workspace;

    public CastLauncherMediaRouterCallback(Activity a){
        activity = a;
    }

    public CastLauncherMediaRouterCallback(Activity a, WorkspacePresentation pres){
        activity = a;
        workspace = pres;
    }

    @Override
    public void onRouteSelected(MediaRouter router, int type, MediaRouter.RouteInfo route) {

    }

    @Override
    public void onRouteUnselected(MediaRouter router, int type, MediaRouter.RouteInfo route) {

    }

    @Override
    public void onRouteAdded(MediaRouter router, MediaRouter.RouteInfo route) {

    }

    @Override
    public void onRouteRemoved(MediaRouter router, MediaRouter.RouteInfo route) {

    }

    @Override
    public void onRoutePresentationDisplayChanged(MediaRouter router, MediaRouter.RouteInfo route) {
        if (route != null) {
            Display display = route.getPresentationDisplay();
            if (display != null) {
                //Create Presentation if it does not already exist.
                if(workspace == null) {
                    workspace = new WorkspacePresentation(activity, display);
                    try {
                        workspace.show();
                    } catch (WindowManager.InvalidDisplayException e) {
                        Toast.makeText(activity, "Cannot show display: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //Remove root view of presentation and add it to the window manager for the presentation display.
                FrameLayout presRootView = (FrameLayout) workspace.findViewById(android.R.id.content);
                ((ViewGroup) presRootView.getParent()).removeView(presRootView);
                Context displayContext = activity.createDisplayContext(display);
                WindowManager displayManager = (WindowManager) displayContext.getSystemService(Context.WINDOW_SERVICE);
                WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, 0, 0, WindowManager.LayoutParams.TYPE_TOAST, 0, PixelFormat.OPAQUE);
                params.setTitle(activity.getString(R.string.presentation_title));
                params.gravity = Gravity.RIGHT | Gravity.TOP;
                displayManager.addView(presRootView, params);
            }
        }
    }

    @Override
    public void onRouteChanged(MediaRouter router, MediaRouter.RouteInfo route) {

    }

    @Override
    public void onRouteGrouped(MediaRouter router, MediaRouter.RouteInfo route, MediaRouter.RouteGroup group, int index) {

    }

    @Override
    public void onRouteUngrouped(MediaRouter router, MediaRouter.RouteInfo route, MediaRouter.RouteGroup group) {

    }

    @Override
    public void onRouteVolumeChanged(MediaRouter router, MediaRouter.RouteInfo route) {

    }
}
