package net.sandstorm.castlauncher;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Brian Wing on 2/14/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class StartMenu {

    private Context context;
    private View rootView;
    private List<AppInfo> appsList;
    private GridView startMenu;
    private EditText searchBar;

    public StartMenu(Context c, View v, final Taskbar taskbar, final List<AppInfo> appsList){
        context = c;
        rootView = v;
        this.appsList = appsList;
        startMenu = (GridView) rootView.findViewById(R.id.start_menu);
        searchBar = (EditText) rootView.findViewById(R.id.start_menu_search);

        //Add apps in list to grid view.
        ArrayAdapter<AppInfo> adapter = new ArrayAdapter<AppInfo>(context, R.layout.start_menu_item, appsList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = LayoutInflater.from(context).inflate(R.layout.start_menu_item, null);
                }

                ImageView iv = (ImageView)convertView.findViewById(R.id.start_menu_app_icon);
                iv.setImageDrawable(appsList.get(position).icon);

                TextView tv = (TextView)convertView.findViewById(R.id.start_menu_app_label);
                tv.setText(appsList.get(position).label);

                return convertView;
            }
        };
        startMenu.setAdapter(adapter);

        //Launch app when clicked from start menu.
        startMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                //Hide start menu after app is launched.
                hide();

                //Launch app.
                PackageManager manager = context.getPackageManager();
                Intent i = manager.getLaunchIntentForPackage(appsList.get(pos).name.toString());
                taskbar.addToTaskbar(appsList.get(pos));
                context.startActivity(i);
            }
        });

        //Enable dragging of icons in start menu.
        startMenu.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ClipData dragData = ClipData.newPlainText("", "");
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(view);
                view.startDrag(dragData, myShadow, view, 0);
                //Hide start menu after app is dragged.
                hide();
                return true;
            }
        });

        //Add text listener for search bar.
        EditText searchBar = (EditText) rootView.findViewById(R.id.start_menu_search);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sortStartMenu(s.toString());
            }
        });

        //Begin with start menu hidden.
        hide();

        //Enable start button.
        ImageButton startButton = (ImageButton) rootView.findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GridView startMenu = (GridView) rootView.findViewById(R.id.start_menu);
                EditText searchBar = (EditText) rootView.findViewById(R.id.start_menu_search);
                if (startMenu.getVisibility() == View.VISIBLE) {
                    startMenu.setVisibility(View.INVISIBLE);
                    searchBar.setVisibility(View.INVISIBLE);
                    searchBar.clearFocus();
                    searchBar.setText("");
                } else {
                    startMenu.setVisibility(View.VISIBLE);
                    searchBar.setVisibility(View.VISIBLE);
                    searchBar.requestFocus();
                }
            }
        });
    }

    /**
     * Set start menu invisble.
     */
    public void hide(){
        startMenu.setVisibility(View.INVISIBLE);
        searchBar.setVisibility(View.INVISIBLE);
        searchBar.clearFocus();
        searchBar.setText("");
    }

    /**
     * Set start menu visible.
     */
    public void show(){
        startMenu.setVisibility(View.VISIBLE);
        searchBar.setVisibility(View.VISIBLE);
    }

    /**
     * Returns the visibility of the start menu.
     * @return The visibility of the start menu.
     */
    public boolean isVisible(){
        if(startMenu.getVisibility() == View.VISIBLE){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Returns the @{link #Bounds bounds} of the start menu and search bar.
     * @return The @{link #Bounds bounds} of the start menu and search bar.
     */
    public Bounds getBounds(){
        return new Bounds(searchBar.getTop(), startMenu.getBottom(), startMenu.getLeft(), startMenu.getRight());
    }

    /**
     * Fills the start menu with only the apps whose name matches the search string provided.
     * @param searchString The search string to sort apps by.
     */
    private void sortStartMenu(String searchString){
        PackageManager manager = context.getPackageManager();
        appsList.clear();
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        //Populate appsList with all available apps that match the search string.
        List<ResolveInfo> allActivities = manager.queryIntentActivities(intent, 0);
        for(ResolveInfo info : allActivities){
            AppInfo app = new AppInfo(info.loadLabel(manager), info.activityInfo.packageName, info.activityInfo.loadIcon(manager));
            if(app.label.length() >= searchString.length()) {
                if (app.label.toString().toLowerCase().contains(searchString.toLowerCase())){
                    appsList.add(app);
                }
            }
        }

        //Add apps from appList to start menu grid view.
        ArrayAdapter<AppInfo> adapter = new ArrayAdapter<AppInfo>(context, R.layout.start_menu_item, appsList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = LayoutInflater.from(context).inflate(R.layout.start_menu_item, null);
                }

                ImageView iv = (ImageView)convertView.findViewById(R.id.start_menu_app_icon);
                iv.setImageDrawable(appsList.get(position).icon);

                TextView tv = (TextView)convertView.findViewById(R.id.start_menu_app_label);
                tv.setText(appsList.get(position).label);

                return convertView;
            }
        };
        ((GridView) rootView.findViewById(R.id.start_menu)).setAdapter(adapter);
    }
}
