package net.sandstorm.castlauncher;

/**
 * Created by Brian Wing on 2/14/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class Bounds {

    private double top;
    private double bottom;
    private double left;
    private double right;

    public Bounds(){
        setTop(0.0);
        setBottom(0.0);
        setLeft(0.0);
        setRight(0.0);
    }

    public Bounds(double top, double bottom, double left, double right){
        setTop(top);
        setBottom(bottom);
        setLeft(left);
        setRight(right);
    }

    public double getTop(){
        return top;
    }

    public double getBottom(){
        return bottom;
    }

    public double getLeft(){
        return left;
    }

    public double getRight(){
        return right;
    }

    public void setTop(double top){
        this.top = top;
    }

    public void setBottom(double bottom){
        this.bottom = bottom;
    }

    public void setLeft(double left){
        this.left = left;
    }

    public void setRight(double right){
        this.right = right;
    }
}
