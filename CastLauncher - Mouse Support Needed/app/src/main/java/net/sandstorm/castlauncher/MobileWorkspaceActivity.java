package net.sandstorm.castlauncher;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.media.MediaRouter;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Created by Brian Wing on 2/14/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class MobileWorkspaceActivity extends Activity{

    WorkspacePresentation workspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.test_mobile_workspace);

        //Add listener for external displays and check if one already exists.
        setupWorkspace();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupWorkspace();
    }

    private void setupWorkspace(){
        MediaRouter router = (MediaRouter) getSystemService(Context.MEDIA_ROUTER_SERVICE);
        MediaRouter.RouteInfo route = router.getDefaultRoute();
        if (route != null) {
            Display display = route.getPresentationDisplay();
            if(display != null){
                //Create Presentation.
                workspace = new WorkspacePresentation(this, display);
                try {
                    workspace.show();
                }catch(WindowManager.InvalidDisplayException e){
                    Toast.makeText(this, "Cannot show display: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                //Remove root view of presentation and add it to the window manager for the presentation display.
                FrameLayout presRootView = (FrameLayout) workspace.findViewById(android.R.id.content);
                ((ViewGroup) presRootView.getParent()).removeView(presRootView);
                Context displayContext = createDisplayContext(display);
                WindowManager displayManager = (WindowManager) displayContext.getSystemService(WINDOW_SERVICE);
                WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, 0, 0, WindowManager.LayoutParams.TYPE_TOAST, 0, PixelFormat.OPAQUE);
                params.setTitle(getString(R.string.presentation_title));
                params.gravity = Gravity.RIGHT | Gravity.TOP;
                displayManager.addView(presRootView, params);

                /**********Optional other way to show external display without Presentation********
                Context displayContext = createDisplayContext(display);
                WindowManager displayManager = (WindowManager) displayContext.getSystemService(WINDOW_SERVICE);
                LayoutInflater inflater = LayoutInflater.from(displayContext);
                RelativeLayout rootView = (RelativeLayout) inflater.inflate(R.layout.workspace_classic, null);
                try {
                    WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, 0, 0, WindowManager.LayoutParams.TYPE_TOAST, 0, PixelFormat.OPAQUE);
                    params.setTitle("Cast Launcher Workspace");
                    params.gravity = Gravity.RIGHT | Gravity.TOP;
                    displayManager.addView(rootView, params);
                    workspace = new Workspace(displayContext, rootView);
                }catch(WindowManager.BadTokenException e){
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                */
            }
        }

        //Register media router listener to detect when the presentation display changes.
        router.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, new CastLauncherMediaRouterCallback(this, workspace));
    }
}
