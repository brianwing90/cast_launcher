package net.sandstorm.castlauncher;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.BaseInputConnection;
import android.widget.ImageButton;
import android.widget.LinearLayout;

/**
 * Created by Brian Wing on 3/8/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class WorkspaceUI extends Service implements View.OnGenericMotionListener {

    Intent intent;

    @Override
    public IBinder onBind(Intent intent) {
        this.intent = intent;
        return null;
    }

    private LinearLayout hitBox;
    private LinearLayout sidebar;
    private WindowManager manager;

    @Override
    public void onCreate() {
        super.onCreate();

        manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams(0, 300, WindowManager.LayoutParams.TYPE_TOAST, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);

        //Add sidebar.
        sidebar = (LinearLayout) ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sidebar_classic, null);
        sidebar.setLayoutParams(layoutParams);
        sidebar.setOnGenericMotionListener(this);
        sidebar.setTag("sidebar");
        windowParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        manager.addView(sidebar, windowParams);

        //Add sidebar buttons.
        ImageButton b = (ImageButton) sidebar.findViewById(R.id.home_btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_MAIN);
                i.addCategory(Intent.CATEGORY_HOME);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                stopSelf();
            }
        });
        b = (ImageButton) sidebar.findViewById(R.id.back_btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Make back button work.
                Intent i = new Intent("key");
                i.putExtra("keyEvent", new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                sendBroadcast(i);
            }
        });

        //Add hitbox.
        hitBox = new LinearLayout(this);
        hitBox.setLayoutParams(layoutParams);
        hitBox.setOnGenericMotionListener(this);
        windowParams = new WindowManager.LayoutParams(5, 1080, WindowManager.LayoutParams.TYPE_TOAST, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
        windowParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        manager.addView(hitBox, windowParams);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if(hitBox != null){
            manager.removeView(hitBox);
        }
        if(sidebar != null){
            manager.removeView(sidebar);
        }
    }

    @Override
    public boolean onGenericMotion(View v, MotionEvent e) {
        if(e.isFromSource(InputDevice.SOURCE_CLASS_POINTER)){
            switch(e.getAction()){
                case MotionEvent.ACTION_HOVER_ENTER:
                    show();
                    break;
                case MotionEvent.ACTION_HOVER_EXIT:
                    //TODO: Make sidebar close if not actively hovering it.
                    if(v.getTag() != null && v.getTag().equals("sidebar")){
                        hide();
                    }
                    break;
                default:
                    break;
            }
        }
        return false;
    }

    private void show(){
        WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams(80, 300, WindowManager.LayoutParams.TYPE_TOAST, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        windowParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        manager.updateViewLayout(sidebar, windowParams);
    }

    private void hide(){
        WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams(0, 300, WindowManager.LayoutParams.TYPE_TOAST, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        windowParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        manager.updateViewLayout(sidebar, windowParams);
    }
}
