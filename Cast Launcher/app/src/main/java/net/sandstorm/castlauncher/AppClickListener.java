package net.sandstorm.castlauncher;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Created by Brian Wing on 3/17/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class AppClickListener implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{

    private HomeActivity activity;
    private int mode;

    public AppClickListener(HomeActivity activity){
        this.activity = activity;
        this.mode = Mode.SHOW_POPUP_MENU;
    }

    private PopupWindow popupWindow;
    private View popupWindowAnchor;

    @Override
    public void onClick(View v) {
        if(this.popupWindow != null && this.popupWindow.isShowing()){
            this.popupWindow.dismiss();
        }

        switch(v.getTag().toString()){
            case "desktop_item_menu_info":
                //TODO: App info window.
                break;
            case "desktop_item_menu_rename":
                //Use hidden editText view to change name of apps on desktop.
                if(popupWindowAnchor != null){
                    final TextView textView = (TextView) this.popupWindowAnchor.findViewById(R.id.desktop_item_label);
                    final EditText editText = (EditText) this.popupWindowAnchor.findViewById(R.id.desktop_item_label_editor);
                    if(textView != null && editText != null){
                        textView.setVisibility(View.GONE);
                        editText.setVisibility(View.VISIBLE);
                        editText.setText(textView.getText());
                        editText.setOnEditorActionListener(new EditText.OnEditorActionListener(){
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                if(actionId == EditorInfo.IME_ACTION_DONE){
                                    textView.setText(editText.getText());
                                    editText.setVisibility(View.GONE);
                                    textView.setVisibility(View.VISIBLE);
                                    InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                    manager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    return true;
                                }
                                return false;
                            }
                        });
                    }
                }
                break;
            case "desktop_item_menu_remove":
                //Show option window to remove app from desktop.
                AlertDialog.Builder dialog = new AlertDialog.Builder(this.activity);
                dialog.setTitle(R.string.alert_remove_app_title);
                dialog.setMessage(R.string.alert_remove_app_message);
                dialog.setPositiveButton(R.string.no, null);
                dialog.setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (popupWindowAnchor != null) {
                            activity.removeFromDesktop(popupWindowAnchor);
                        }
                    }
                });
                //TODO: Change size of dialog window.
                dialog.create();
                dialog.show();
                break;
            case "desktop_item_menu_uninstall":
                //Show option window to remove app from phone.
                TextView packageName = (TextView) popupWindowAnchor.findViewById(R.id.desktop_item_package_name);
                Uri packageURI = Uri.parse("package:" + packageName.getText().toString());
                Intent i = new Intent(Intent.ACTION_DELETE, packageURI);
                activity.startActivity(i);
                break;
            case "desktop_item":
                //Launch app on click from desktop icon.
                String label = ((TextView) v.findViewById(R.id.desktop_item_label)).getText().toString();
                Drawable icon = ((ImageView) v.findViewById(R.id.desktop_item_icon)).getDrawable();
                for (App app : activity.getAppsList()) {
                    if (app.getLabel().equals(label) && app.getIcon().getConstantState().equals(icon.getConstantState())) {
                        this.activity.launchApp(app);
                        return;
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(mode == Mode.SHOW_POPUP_MENU) {
            if (this.popupWindow == null) {
                ViewGroup popupMenu = (ViewGroup) this.activity.getLayoutInflater().inflate(R.layout.desktop_item_popup_menu, null);

                //Add onTouch listeners for menu items.
                for (int i = 0; i < popupMenu.getChildCount(); i++) {
                    View view = popupMenu.getChildAt(i);
                    if (view instanceof TextView) {
                        view.setOnClickListener(this);
                    }
                }

                popupMenu.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                this.popupWindow = new PopupWindow(popupMenu, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                this.popupWindow.setBackgroundDrawable(this.activity.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            }
            this.popupWindow.showAsDropDown(v, v.getWidth() / 2, -v.getHeight() / 2);
            this.popupWindowAnchor = v;
        }else if(mode == Mode.MOVE_ICONS){
            ClipData dragData = ClipData.newPlainText("", "");
            View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
            v.startDrag(dragData, myShadow, v, 0);
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
        //Launch app.
        this.activity.launchApp(this.activity.getStartMenuAppsList().get(pos));
        this.activity.hideStartMenu();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ClipData dragData = ClipData.newPlainText("", "");
        View.DragShadowBuilder myShadow = new View.DragShadowBuilder(view);
        view.startDrag(dragData, myShadow, view, 0);
        //Hide start menu after app is dragged.
        this.activity.hideStartMenu();
        return true;
    }

    public void setMode(int mode){
        switch(mode){
            case Mode.SHOW_POPUP_MENU:
                this.mode = Mode.SHOW_POPUP_MENU;
                break;
            case Mode.MOVE_ICONS:
                this.mode = Mode.MOVE_ICONS;
                break;
            default:
                this.mode = Mode.SHOW_POPUP_MENU;
                break;
        }
    }
}
