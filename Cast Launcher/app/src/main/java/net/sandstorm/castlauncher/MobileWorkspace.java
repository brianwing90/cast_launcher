package net.sandstorm.castlauncher;

import android.graphics.PointF;
import android.graphics.RectF;
import android.support.v4.view.ViewPager;
import android.view.DragEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Brian Wing on 3/25/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class MobileWorkspace implements View.OnDragListener, View.OnClickListener, View.OnLongClickListener{

    private HomeActivity activity;

    public MobileWorkspace(HomeActivity a){
        this.activity = a;

        //Setup app pages.
        ViewPager viewPager = (ViewPager) this.activity.findViewById(R.id.mobile_app_page_holder);
        MobilePagerAdapter adapter = new MobilePagerAdapter(this.activity);
        viewPager.setAdapter(adapter);

        //TODO: Populate app pages with apps/widgets.
        //TODO: Add drag listeners for app pages.
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * Handle dragging of app icons.
     * @param v The view being dragged to.
     * @param event The {@link DragEvent DragEvent} started when the item drag is started.
     * @return True if the drag event was consumed here, false otherwise.
     */
    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                //Hide view being dragged while being dragged.
                RelativeLayout view = (RelativeLayout) event.getLocalState();
                view.setVisibility(View.GONE);
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                //Handle the dragged view being dropped over a drop view.
                view = (RelativeLayout) event.getLocalState();
                TextView packageName = (TextView) view.findViewWithTag("package_name");
                TextView label = (TextView) view.findViewWithTag("label");
                ImageView icon = (ImageView) view.findViewWithTag("icon");
                if (label != null && icon != null) {
                    App app = new App(label.getText().toString(), packageName.getText().toString(), icon.getDrawable());
                    PointF p = new PointF(event.getX(), event.getY());
                    add(app, p);
                } else {
                    return false;
                }

                //Remove old view from desktop once drag is completed.
                remove(view);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                //Unhide view if it is dropped somewhere other than on the desktop.
                view = (RelativeLayout) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private void add(App app, PointF point){
        //TODO: Add apps to app page.
        //Create app view for desktop.
        View desktopItem = this.activity.getLayoutInflater().inflate(R.layout.desktop_item, null);
        ImageView icon = (ImageView) desktopItem.findViewById(R.id.desktop_item_icon);
        icon.setImageDrawable(app.getIcon());
        TextView label = (TextView) desktopItem.findViewById(R.id.desktop_item_label);
        label.setText(app.getLabel());
        TextView packageName = (TextView) desktopItem.findViewById(R.id.desktop_item_package_name);
        packageName.setText(app.getName());
        desktopItem.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        //Keep icon within screen bounds.
        RectF child = new RectF(point.x - desktopItem.getMeasuredWidth(), point.y - desktopItem.getMeasuredHeight(), point.x , point.y);
        //this.desktop.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //RectF parent = new RectF(0.0f, 0.0f, this.desktop.getMeasuredWidth() - 60, this.desktop.getMeasuredHeight() - 60);
        //child = clampToParent(child, parent);
        desktopItem.setX(child.left);
        desktopItem.setY(child.top);
        desktopItem.setTag(app.getName());

        //Enable dragging of icons on desktop.
        //desktopItem.setOnLongClickListener(appListener);

        //Enable launching app from desktop icon.
        //desktopItem.setOnClickListener(appListener);
        //desktopItem.setTag("desktop_item");

        //this.desktop.addView(desktopItem);
    }

    private void remove(View view){

    }
}