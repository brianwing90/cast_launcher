package net.sandstorm.castlauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by Brian Wing on 2/24/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class App {

    private String label;
    private String name;
    private Drawable icon;

    public App(){
        setLabel("");
        setName("");
        setIcon(null);
    }

    public App(String label, String name, Drawable icon){
        setLabel(label);
        setName(name);
        setIcon(icon);
    }

    public String getLabel(){
        return label;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Drawable getIcon(){
        return icon;
    }

    public void setIcon(Drawable icon){
        this.icon = icon;
    }
}

