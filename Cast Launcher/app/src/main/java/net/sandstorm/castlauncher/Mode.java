package net.sandstorm.castlauncher;

/**
 * Created by Brian Wing on 2/24/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class Mode {
    public static final int PHONE = 0;
    public static final int DESKTOP = 1;
    public static final int SHOW_POPUP_MENU = 2;
    public static final int MOVE_ICONS = 3;
}
