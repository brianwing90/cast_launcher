package net.sandstorm.castlauncher;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian Wing on 2/24/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class Workspace implements View.OnDragListener, View.OnClickListener, View.OnLongClickListener, TextWatcher{

    private static final String SAVE_STATE_FILE_NAME = "desktop.dat";

    private HomeActivity activity;
    private ImageView wallpaper;
    private RelativeLayout desktop;
    private LinearLayout taskbar;
    private AppClickListener appListener;
    private List<App> taskbarAppsList;
    private GridView startMenu;
    private EditText startMenuSearchBar;
    private List<App> startMenuAppsList;

    public Workspace(HomeActivity a){
        this.activity = a;

        //Load wallpaper.
        //TODO: Load and save wallpaper choice with desktop apps.
        this.wallpaper = (ImageView) this.activity.findViewById(R.id.desktop_wallpaper);
        WallpaperManager wpManager = WallpaperManager.getInstance(this.activity);
        this.wallpaper.setBackground(wpManager.getDrawable());

        //Load desktop.
        this.desktop = (RelativeLayout) this.activity.findViewById(R.id.desktop);
        this.desktop.setOnDragListener(this);
        this.desktop.setOnLongClickListener(this);
        appListener = new AppClickListener(this.activity);
        loadState();

        //Load taskbar.
        this.taskbar = (LinearLayout) this.activity.findViewById(R.id.taskbar);
        this.taskbarAppsList = new ArrayList<App>();

        //Load start menu.
        this.startMenu = (GridView) this.activity.findViewById(R.id.start_menu);
        this.startMenuSearchBar = (EditText) this.activity.findViewById(R.id.start_menu_search);
        this.startMenuAppsList = getAppsList();

        //Add apps in list to grid view.
        sortStartMenu("");

        //Launch app when clicked from start menu.
        this.startMenu.setOnItemClickListener(appListener);

        //Enable dragging of icons in start menu.
        this.startMenu.setOnItemLongClickListener(appListener);

        //Add text listener for search bar.
        this.startMenuSearchBar.addTextChangedListener(this);

        //Begin with start menu hidden.
        hideStartMenu();
    }

    /**************************************************************************
     ***************************** DESKTOP METHODS ****************************
     **************************************************************************/

    /**
     * Handle dragging of items on the desktop. Particularly adding icons to the desktop from the start menu and moving items around on the desktop.
     * @param v The view being dragged to.
     * @param event The {@link DragEvent DragEvent} started when the item drag is started.
     * @return True if the drag event was consumed here, false otherwise.
     */
    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                //Hide view being dragged while being dragged.
                RelativeLayout view = (RelativeLayout) event.getLocalState();
                view.setVisibility(View.GONE);
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                //Handle the dragged view being dropped over a drop view.
                view = (RelativeLayout) event.getLocalState();
                TextView packageName = (TextView) view.findViewWithTag("package_name");
                TextView label = (TextView) view.findViewWithTag("label");
                ImageView icon = (ImageView) view.findViewWithTag("icon");
                if (label != null && icon != null) {
                    App app = new App(label.getText().toString(), packageName.getText().toString(), icon.getDrawable());
                    PointF p = new PointF(event.getX(), event.getY());
                    addToDesktop(app, p);
                } else {
                    return false;
                }

                //Remove old view from desktop once drag is completed.
                removeFromDesktop(view);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                //Unhide view if it is dropped somewhere other than on the desktop.
                view = (RelativeLayout) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        return true;
    }

    private PopupWindow popupWindow;

    @Override
    public void onClick(View v) {
        if(this.popupWindow != null && this.popupWindow.isShowing()){
            this.popupWindow.dismiss();
        }

        switch(v.getTag().toString()){
            case "desktop_menu_move":
                //Gray out and disable taskbar and start menu.
                RelativeLayout dock = (RelativeLayout) this.activity.findViewById(R.id.dock);
                dock.setAlpha(.5f);
                for(int i = 0; i < this.taskbar.getChildCount(); i++){
                    this.taskbar.getChildAt(i).setOnClickListener(null);
                    this.taskbar.getChildAt(i).setOnLongClickListener(null);
                }
                ImageButton startButton = (ImageButton) dock.findViewById(R.id.start_button);
                startButton.setEnabled(false);
                this.appListener.setMode(Mode.MOVE_ICONS);
                this.popupWindow.getContentView().findViewById(R.id.desktop_menu_move).setVisibility(View.GONE);
                this.popupWindow.getContentView().findViewById(R.id.desktop_menu_stop_move).setVisibility(View.VISIBLE);
                break;
            case "desktop_menu_stop_move":
                //Unhide and enable taskbar and start menu.
                dock = (RelativeLayout) this.activity.findViewById(R.id.dock);
                dock.setAlpha(1.0f);
                for(int i = 0; i < this.taskbar.getChildCount(); i++){
                    this.taskbar.getChildAt(i).setOnClickListener(this.taskbarListener);
                    this.taskbar.getChildAt(i).setOnLongClickListener(this.taskbarListener);
                }
                startButton = (ImageButton) dock.findViewById(R.id.start_button);
                startButton.setEnabled(true);
                this.appListener.setMode(Mode.SHOW_POPUP_MENU);
                this.popupWindow.getContentView().findViewById(R.id.desktop_menu_move).setVisibility(View.VISIBLE);
                this.popupWindow.getContentView().findViewById(R.id.desktop_menu_stop_move).setVisibility(View.GONE);
                break;
            case "desktop_menu_background":
                //Create intent to get desktop background.
                //TODO: Set background image correctly.
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                this.activity.startActivityForResult(Intent.createChooser(intent, "Select Background Image"), this.activity.GET_DESKTOP_BACKGROUND);
                break;
            default:
                break;
        }
    }

    private int mouseX;
    private int mouseY;

    @Override
    public boolean onLongClick(View v) {
        ViewGroup popupMenu = (ViewGroup) this.activity.getLayoutInflater().inflate(R.layout.desktop_popup_menu, null);
        popupMenu.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        if(this.popupWindow == null){
            //Add onTouch listeners for menu items.
            for(int i = 0; i < popupMenu.getChildCount(); i++){
                View view = popupMenu.getChildAt(i);
                if(view instanceof TextView){
                    view.setOnClickListener(this);
                }
            }
            this.popupWindow = new PopupWindow(popupMenu, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
            this.popupWindow.setBackgroundDrawable(this.activity.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
        }

        Display display = this.activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.popupWindow.showAsDropDown(v, mouseX, mouseY - size.y + popupMenu.getMeasuredHeight() / 2); // Correctly position desktop popup window.
        return true;
    }

    /**
     * Refresh values of mouseX and mouseY for use in the onLongClick method. This helps correctly position the long click menu of the desktop.
     * @param x The x coordinate of the mouse.
     * @param y The y coordinate of the mouse.
     */
    public void updateMouseLocation(int x, int y){
        this.mouseX = x;
        this.mouseY = y;
    }

    /**
     * Creates a drawable out of the given string path and sets it to the wallpaper element of the dekstop.
     * @param imagePath The path of the image to set as the background.
     */
    public void setDesktopBackground(String imagePath){
        if(!imagePath.equals("") && imagePath != null) {
            this.wallpaper.setBackground(Drawable.createFromPath(imagePath));
        }
    }

    /**
     * Sets the given drawable to the wallpaper element of the dekstop.
     * @param image The drawable to set as the background.
     */
    public void setDesktopBackground(Drawable image){
        if(image != null) {
            this.wallpaper.setBackground(image);
        }
    }

    /**
     * Save the current desktop icon configuration for loading next time the app is started.
     */
    public void saveState(){
        FileOutputStream out = null;
        try {
            //TODO: SD card support.
            out = this.activity.openFileOutput(SAVE_STATE_FILE_NAME, Context.MODE_PRIVATE);

            String output = "";
            //Fill output string with app data from desktop.
            for(int i = 0; i < this.desktop.getChildCount(); i++){
                output += ((TextView) this.desktop.getChildAt(i).findViewById(R.id.desktop_item_label)).getText() + ",";
                output += this.desktop.getChildAt(i).getTag() + ",";
                int[] pos = new int[2];
                this.desktop.getChildAt(i).getLocationOnScreen(pos);
                output += pos[0] + ",";
                output += pos[1] + "\n";
            }

            out.write(output.getBytes());
            out.close();
        } catch (Exception e) {
            Toast.makeText(this.activity, R.string.file_write_error, Toast.LENGTH_LONG).show();
        } finally {
            if(out != null) {
                try {
                    out.close();
                }catch(IOException e){

                }
            }
        }
    }

    /**
     * Load the last used desktop icon configuration.
     */
    private void loadState(){
        File f = new File(this.activity.getFilesDir(), SAVE_STATE_FILE_NAME);
        //TODO: SD card support.

        if(f.exists()){
            BufferedReader in = null;
            try{
                in = new BufferedReader(new InputStreamReader(this.activity.openFileInput(SAVE_STATE_FILE_NAME)));

                String input = "";
                while((input = in.readLine()) != null){
                    String[] parsedInput = input.split(",");
                    App app = new App(parsedInput[0], parsedInput[1], null);
                    app = findCompleteApp(app);
                    if(app != null){
                        PointF p = new PointF(Integer.parseInt(parsedInput[2]), Integer.parseInt(parsedInput[3]));
                        addToDesktop(app, p);
                    }
                }

                in.close();
            }catch(Exception e){
                Toast.makeText(this.activity, R.string.file_read_error, Toast.LENGTH_LONG).show();
            }finally {
                if(in != null) {
                    try {
                        in.close();
                    }catch(IOException e){

                    }
                }
            }
        }
    }

    /**
     * Add an {@link App App} to the desktop.
     * @param app The app to add to the desktop.
     * @param point The place to add the app.
     */
    private void addToDesktop(App app, PointF point){
        //Create app view for desktop.
        View desktopItem = this.activity.getLayoutInflater().inflate(R.layout.desktop_item, null);
        ImageView icon = (ImageView) desktopItem.findViewById(R.id.desktop_item_icon);
        icon.setImageDrawable(app.getIcon());
        TextView label = (TextView) desktopItem.findViewById(R.id.desktop_item_label);
        label.setText(app.getLabel());
        TextView packageName = (TextView) desktopItem.findViewById(R.id.desktop_item_package_name);
        packageName.setText(app.getName());
        desktopItem.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        //Keep icon within screen bounds.
        RectF child = new RectF(point.x - desktopItem.getMeasuredWidth(), point.y - desktopItem.getMeasuredHeight(), point.x , point.y);
        this.desktop.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        RectF parent = new RectF(0.0f, 0.0f, this.desktop.getMeasuredWidth() - 60, this.desktop.getMeasuredHeight() - 60);
        child = clampToParent(child, parent);
        desktopItem.setX(child.left);
        desktopItem.setY(child.top);
        desktopItem.setTag(app.getName());

        //Enable dragging of icons on desktop.
        desktopItem.setOnLongClickListener(appListener);

        //Enable launching app from desktop icon.
        desktopItem.setOnClickListener(appListener);
        desktopItem.setTag("desktop_item");

        this.desktop.addView(desktopItem);
    }

    public void removeFromDesktop(View view){
        if(this.desktop.indexOfChild(view) != -1){
            this.desktop.removeView(view);
        }
    }

    /**
     * Clamps the given child {@link RectF rectangle} to the bounds of the given parent rectangle.
     * @param child The rectangular bounds of the object being drawn in the parent.
     * @param parent The rectangular bounds of the object being drawn in the parent.
     * @return The modified child rectangle to draw in the parent.
     */
    private static RectF clampToParent(RectF child, RectF parent){
        //Clamp x's.
        if(child.right > parent.right){
            double xDist = Math.sqrt(Math.pow(child.right - parent.right, 2));
            child.left -= xDist;
            child.right -= xDist;
        }else if(child.left < parent.left){
            double xDist = Math.sqrt(Math.pow(parent.left - child.left, 2));
            child.left += xDist;
            child.right += xDist;
        }

        //Clamp y's.
        if(child.top < parent.top){
            double yDist = Math.sqrt(Math.pow(child.top - parent.top, 2));
            child.top += yDist;
            child.bottom += yDist;
        }else if(child.bottom > parent.bottom){
            double yDist = Math.sqrt(Math.pow(parent.bottom - child.bottom, 2));
            child.top -= yDist;
            child.bottom -= yDist;
        }

        return child;
    }

    /**
     * Returns a list of all apps installed on the device as a {@link App App} list.
     * @return A list of all apps installed on the device.
     */
    public List<App> getAppsList(){
        List<App> appsList = new ArrayList<App>();
        //Populate appList with all available apps.
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        PackageManager manager = activity.getPackageManager();
        List<ResolveInfo> allActivities = manager.queryIntentActivities(i, 0);
        for(ResolveInfo info : allActivities){
            App app = new App(info.loadLabel(manager).toString(), info.activityInfo.packageName, info.activityInfo.loadIcon(manager));
            appsList.add(app);
        }

        return appsList;
    }

    /**
     * Returns an {@link App app} from the list of apps installed on the device.
     * @param app A partially complete app to search for.
     * @return The complete app, with name, label, and icon.
     */
    private App findCompleteApp(App app){
        App result = null;

        List<App> appsList = getAppsList();
        for(App appInList : appsList){
            if(appInList.getName().equals(app.getName()) && appInList.getLabel().equals(app.getLabel())){
                result = appInList;
                break;
            }
        }

        return result;
    }

    /**************************************************************************
     ***************************** TASKBAR METHODS ****************************
     **************************************************************************/

    /**
     * Adds an app to the list of apps to be displayed on the taskbar and fills the taskbar with those apps.
     * @param app The app to be added to the taskbar view.
     */
    public void addToTaskbar(App app){
        //Check if app is already in taskbar list.
        for(int i = 0; i < this.taskbarAppsList.size(); i++){
            if(app == null){
                break;
            }
            //Check if the new app and any other app are the same in either name, label, or icon.
            App appAtI = this.taskbarAppsList.get(i);
            if(appAtI.getLabel().equals(app.getLabel()) && appAtI.getName().equals(app.getName()) && appAtI.getIcon().getConstantState().equals(app.getIcon().getConstantState())){
                app = null;
            }
        }

        if(app != null){
            this.taskbarAppsList.add(app);
        }

        refreshTaskbar();
    }

    public void removeFromTaskbar(View v){
        if(this.taskbar.indexOfChild(v) != -1){
            this.taskbar.removeView(v);
        }
    }

    private TaskbarClickListener taskbarListener;

    /**
     * Refreshes the view of the taskbar to reflect any recent changes and add click listeners for taskbar items.
     */
    private void refreshTaskbar(){
        if(taskbarListener == null){
            taskbarListener = new TaskbarClickListener(this.activity);
        }

        //Add apps in taskbarAppsList to the taskbar view.
        this.taskbar.removeAllViews();
        for(int i = 0; i < this.taskbarAppsList.size(); i++){
            View taskbarItem = activity.getLayoutInflater().inflate(R.layout.taskbar_item, null);
            ImageView iv = (ImageView) taskbarItem.findViewById(R.id.taskbar_item_icon);
            iv.setImageDrawable(this.taskbarAppsList.get(i).getIcon());
            TextView tv = (TextView) taskbarItem.findViewById(R.id.taskbar_item_label);
            tv.setText(this.taskbarAppsList.get(i).getLabel());

            //Add click listeners to taskbar item.
            taskbarItem.setOnClickListener(taskbarListener);
            taskbarItem.setOnLongClickListener(taskbarListener);

            taskbar.addView(taskbarItem);
        }
    }

    /**
     * Returns the list of apps in the taskbar.
     * @return The list of apps in the taskbar.
     */
    public List<App> getTaskbarAppsList(){
        return this.taskbarAppsList;
    }

    /**************************************************************************
     ************************** START MENU METHODS ****************************
     **************************************************************************/

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        sortStartMenu(s.toString());
    }

    /**
     * Set start menu invisible.
     */
    public void hideStartMenu(){
        this.startMenu.setVisibility(View.INVISIBLE);
        this.startMenuSearchBar.setVisibility(View.INVISIBLE);
        this.startMenuSearchBar.clearFocus();
        this.startMenuSearchBar.setText("");
    }

    /**
     * Hide the start menu if the given motion event is outside of the start menu bounds.
     * @param ev The motion event that indicated that the start menu should become hidden.
     */
    public void hideStartMenu(MotionEvent ev){
        if(this.startMenu.getVisibility() == View.VISIBLE) {
            Bounds bounds = getStartMenuBounds();
            if (ev.getY() < bounds.getTop() || ev.getY() > bounds.getBottom() || ev.getX() > bounds.getRight() || ev.getX() < bounds.getLeft()) {
                //TODO: Check if click is not within start menu button.
                hideStartMenu();
            }
        }
    }

    /**
     * Set start menu visible.
     */
    public void showStartMenu(){
        this.startMenu.setVisibility(View.VISIBLE);
        this.startMenuSearchBar.setVisibility(View.VISIBLE);
        this.startMenuSearchBar.requestFocus();
    }

    /**
     * Returns the @{link #Bounds bounds} of the start menu and search bar.
     * @return The @{link #Bounds bounds} of the start menu and search bar.
     */
    private Bounds getStartMenuBounds(){
        return new Bounds(this.startMenuSearchBar.getTop(), this.startMenu.getBottom(), this.startMenu.getLeft(), this.startMenu.getRight());
    }

    public List<App> getStartMenuAppsList(){
        return startMenuAppsList;
    }

    /**
     * Fills the start menu with only the apps whose name matches the search string provided.
     * @param searchString The search string to sort apps by.
     */
    private void sortStartMenu(String searchString){
        PackageManager manager = this.activity.getPackageManager();
        this.startMenuAppsList.clear();
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        //Populate appsList with all available apps that match the search string.
        List<ResolveInfo> allActivities = manager.queryIntentActivities(intent, 0);
        for(ResolveInfo info : allActivities){
            App app = new App(info.loadLabel(manager).toString(), info.activityInfo.packageName, info.activityInfo.loadIcon(manager));
            if(app.getLabel().length() >= searchString.length()) {
                if (app.getLabel().toLowerCase().contains(searchString.toLowerCase())){
                    this.startMenuAppsList.add(app);
                }
            }
        }

        //Add apps from appList to start menu grid view.
        ArrayAdapter<App> adapter = new ArrayAdapter<App>(this.activity, R.layout.start_menu_item, this.startMenuAppsList){
            @Override
            public View getView(int position, View child, ViewGroup parent) {
                if(child == null){
                    child = activity.getLayoutInflater().inflate(R.layout.start_menu_item, null);
                }

                //Set the name of the app in the view.
                TextView packageName = (TextView) child.findViewById(R.id.start_menu_item_package_name);
                packageName.setText(startMenuAppsList.get(position).getName());

                //Set the icon of the app in the view.
                ImageView icon = (ImageView) child.findViewById(R.id.start_menu_item_icon);
                icon.setImageDrawable(startMenuAppsList.get(position).getIcon());

                //Set the label of the app in the view.
                TextView label = (TextView) child.findViewById(R.id.start_menu_item_label);
                label.setText(startMenuAppsList.get(position).getLabel());

                return child;
            }
        };
        ((GridView) this.activity.findViewById(R.id.start_menu)).setAdapter(adapter);
    }
}