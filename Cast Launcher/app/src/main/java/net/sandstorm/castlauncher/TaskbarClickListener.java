package net.sandstorm.castlauncher;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Brian Wing on 3/24/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class TaskbarClickListener implements View.OnClickListener, View.OnLongClickListener{

    private HomeActivity activity;

    public TaskbarClickListener(HomeActivity activity){
        this.activity = activity;
    }

    private PopupWindow popupWindow;
    private View popupWindowAnchor;

    @Override
    public void onClick(View v) {
        if(this.popupWindow != null && this.popupWindow.isShowing()){
            this.popupWindow.dismiss();
        }

        switch(v.getTag().toString()){
            case "taskbar_item_menu_info":
                //TODO: App info window.
                break;
            case "taskbar_item_menu_close":
                //Remove app from taskbar.
                this.activity.removeFromTaskbar(popupWindowAnchor);
                break;
            case "taskbar_item":
                App app = new App();
                app.setIcon(((ImageView) v.findViewById(R.id.taskbar_item_icon)).getDrawable());
                app.setLabel(((TextView) v.findViewById(R.id.taskbar_item_label)).getText().toString());

                List<App> appsList = this.activity.getTaskbarAppsList();
                for(int i = 0; i < appsList.size(); i++){
                    if(appsList.get(i).getLabel().equals(app.getLabel()) && appsList.get(i).getIcon().getConstantState().equals(app.getIcon().getConstantState())){
                        app = appsList.get(i);
                    }
                }

                //Launch app.
                if(!app.getName().equals("")){
                    PackageManager manager = activity.getPackageManager();
                    Intent i = manager.getLaunchIntentForPackage(app.getName());
                    activity.startActivity(i);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        ViewGroup popupMenu = (ViewGroup) this.activity.getLayoutInflater().inflate(R.layout.taskbar_item_popup_menu, null);
        popupMenu.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        if(this.popupWindow == null) {
            //Add onTouch listeners for menu items.
            for (int i = 0; i < popupMenu.getChildCount(); i++) {
                View view = popupMenu.getChildAt(i);
                if (view instanceof TextView) {
                    view.setOnClickListener(this);
                }
            }

            this.popupWindow = new PopupWindow(popupMenu, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
            this.popupWindow.setBackgroundDrawable(this.activity.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
        }
        this.popupWindow.showAsDropDown(v, v.getWidth() / 2, -v.getHeight() / 2 - popupMenu.getMeasuredHeight());
        this.popupWindowAnchor = v;
        return true;
    }
}
