package net.sandstorm.castlauncher;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewSwitcher;

import java.util.List;

/**
 * Created by Brian Wing on 2/24/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class HomeActivity extends Activity{

    public final static int GET_DESKTOP_BACKGROUND = 1;

    private int mode = Mode.PHONE;
    private ViewSwitcher switcher;
    private Workspace workspace;
    private MobileWorkspace mobileWorkspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.workspace_classic);
        switcher = (ViewSwitcher) findViewById(R.id.workspace_switcher);
        workspace = new Workspace(this);
        mobileWorkspace = new MobileWorkspace(this);

        if(savedInstanceState != null) {
            mode = savedInstanceState.getInt("mode");
        }else{
            mode = Mode.PHONE;
        }
        switchMode(null);
    }

    @Override
    protected void onPause(){
        super.onPause();

        if(mode == Mode.DESKTOP){
            startService(new Intent(this, WorkspaceUI.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(this, WorkspaceUI.class));
    }

    @Override
    protected void onStop(){
        super.onStop();
        workspace.saveState();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("mode", mode);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if(mode == Mode.DESKTOP) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.alert_switch_mode_title);
            dialog.setMessage(R.string.alert_switch_mode_message);
            dialog.setPositiveButton(R.string.no, null);
            dialog.setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //HomeActivity.super.onBackPressed();
                    switchMode(null);
                }
            });
            //TODO: Change size of dialog window.
            dialog.create();
            dialog.show();
        }
    }

    /**
     * Switch between mobile and desktop views.
     * @param v The {@link View View} that called the method. Usually a button.
     */
    public void switchMode(View v){
        if(mode == Mode.PHONE){
            mode = Mode.DESKTOP;
            switcher.showNext();
        }else if(mode == Mode.DESKTOP){
            mode = Mode.PHONE;
            switcher.showPrevious();
        }
    }

    /**
     * Start an app and add the handle of it to the taskbar.
     * @param app The {@link App App} to start.
     */
    public void launchApp(App app){
        PackageManager packageManager = getPackageManager();
        Intent i = packageManager.getLaunchIntentForPackage(app.getName());
        if(mode == Mode.DESKTOP){
            workspace.addToTaskbar(app);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ActivityOptions options = ActivityOptions.makeBasic();
                options.setLaunchBounds(new Rect(0, 0, 400, 600));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Open a new window for each task launched.
                i.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                startActivity(i, options.toBundle());
            }else {
                startActivity(i);
            }
        }else{
            startActivity(i);
        }

    }

    /**************************************************************************
     ***************************** DESKTOP METHODS ****************************
     **************************************************************************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == GET_DESKTOP_BACKGROUND) {
                Uri imgURI = data.getData();
                workspace.setDesktopBackground(imgURI.getPath());
            }
        }
    }

    /**
     * Show the start menu on the desktop.
     * @param v The {@link View View} that called this method. Usually a button.
     */
    public void showStartMenu(View v){
        workspace.showStartMenu();
    }

    public void hideStartMenu(){
        workspace.hideStartMenu();
    }

    public List<App> getAppsList(){
        return workspace.getAppsList();
    }

    public List<App> getStartMenuAppsList(){
        return workspace.getStartMenuAppsList();
    }

    public List<App> getTaskbarAppsList(){
        return workspace.getTaskbarAppsList();
    }

    public void removeFromDesktop(View view){
        workspace.removeFromDesktop(view);
    }

    public void removeFromTaskbar(View view){
        workspace.removeFromTaskbar(view);
    }

    /**
     * Override dispatchTouchEvent method to provide closing functionality for the start menu when a touch is detected outside of the bounds of the menu.
     * @param ev The touch event.
     */
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(mode == Mode.DESKTOP && ev.getAction() == MotionEvent.ACTION_DOWN){
            workspace.hideStartMenu(ev);
            workspace.updateMouseLocation((int) ev.getX(), (int) ev.getY());
        }
        return super.dispatchTouchEvent(ev);
    }

    /**************************************************************************
     ***************************** MOBILE METHODS *****************************
     **************************************************************************/
}