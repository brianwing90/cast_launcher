package net.sandstorm.castlauncher;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by Brian Wing on 3/25/2016.
 * Sandstorm Technologies
 * brian@sandstorm.net
 */
public class MobilePagerAdapter extends PagerAdapter{

    private HomeActivity activity;
    private int pageCount;

    public MobilePagerAdapter(HomeActivity a){
        //super(a.getFragmentManager());
        this.activity = a;
        this.pageCount = 0;
    }

    @Override
    public Object instantiateItem(ViewGroup group, int position) {
        LinearLayout appPage = (LinearLayout) this.activity.getLayoutInflater().inflate(R.layout.mobile_app_page, null);
        group.addView(appPage); //The "group" is the ViewPager.
        this.pageCount++;
        return appPage;
    }

    @Override
    public void destroyItem(ViewGroup group, int position, Object view) {
        group.removeView((LinearLayout) view); //The "group" is the ViewPager.
        this.pageCount--;
    }

    @Override
    public int getCount() {
        return this.pageCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
